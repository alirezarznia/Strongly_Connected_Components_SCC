#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;

using namespace std;
vector<ll>adj[25];ll n , m;
bool vis[25];
vector<ll>cadj[25];
ll DFS(ll x , stack <ll>&st){
    vis[x]=true;
    for(auto i: adj[x])
        if(!vis[i]) DFS(i ,st);
    st.push(x);
    return 0;
}
ll DFST(ll x){
    vis[x]=true;
    for(auto i: cadj[x])
        if(!vis[i]) DFST(i);
    return 0;
}
ll cop(){
    Rep(i , n){
        for(auto j : adj[i])
            cadj[j].push_back(i);
    }
    return 0;
}
ll SCC(){
    stack <ll>st;
    Rep(i , n){
        if(!vis[i])
            DFS(i , st);
    }
    Set(vis, false);
    cop();
    ll cnt =0 ;
    while(!st.empty()){
        ll q = st.top();
        st.pop();
        if(!vis[q])
            DFST(q) ,cnt++;
    }
    return cnt;
}
int main(){
   // Test;
    ll t ; cin>>t;
    while(t--){
        cin>>n>>m;
        Rep(i, n){   adj[i].clear();vis[i]=false;cadj[i].clear(); }
        Rep(i ,n){
            ll x, y; cin>>x>>y;x-- , y--;
            adj[x].push_back(y);
        }
        cout<<SCC()<<endl;
    }
    return 0;
}